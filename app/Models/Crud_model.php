<?php

namespace App\Models;
use CodeIgniter\Model;

class Crud_model extends Model
{
    public function retrieve_where($table , $data){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $builder->where($data);
        $query   = $builder->get();
        return $query->getResult();
    }
    public function retrieve($table){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $query   = $builder->get();
        return $query->getResult();
    }
    public function retrieve_single($table , $data){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $builder->where($data);
        $query   = $builder->get();
        return $query->getResult();
    }
    public function create($table , $data){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $query   = $builder->insert($data);
        return $query;
    }
    public function update_where($table , $data , $condition){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $builder->where($condition);
        $query   = $builder->update($data);
        return $query;
    }
}
