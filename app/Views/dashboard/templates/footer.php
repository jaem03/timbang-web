<footer>
          <div class="pull-right">
            Timbang App
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    
    <!-- Bootstrap -->
   <script src="<?= base_url('assets/template') ?>/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url('assets/template') ?>/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?= base_url('assets/template') ?>/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?= base_url('assets/template') ?>/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?= base_url('assets/template') ?>/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- morris.js -->
    <script src="<?= base_url('assets/template') ?>/vendors/raphael/raphael.min.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/morris.js/morris.min.js"></script>
    <!-- gauge.js -->
    <script src="<?= base_url('assets/template') ?>/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?= base_url('assets/template') ?>/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Skycons -->
    <script src="<?= base_url('assets/template') ?>/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?= base_url('assets/template') ?>/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?= base_url('assets/template') ?>/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?= base_url('assets/template') ?>/vendors/moment/min/moment.min.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?= base_url('assets/template') ?>/build/js/custom.min.js"></script>
  