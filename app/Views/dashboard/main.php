

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row" style="display: inline-block;">
            <div class=" top_tiles" style="margin: 10px 0;">
              <div class="col-md-2 col-sm-2  tile">
                <span>Underweight Childrens</span>
                <h2>0</h2>
                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
              </div>
              <div class="col-md-2 col-sm-2  tile">
                <span>Obese Childrens</span>
                <h2>0</h2>
                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
              </div>
              <div class="col-md-2 col-sm-2  tile">
                <span>Stunted Childrens</span>
                <h2>0</h2>
                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 125px;"></canvas>
                  </span> 
              </div>
              <div class="col-md-2 col-sm-2  tile">
                <span>Normal Childrens</span>
                <h2>0</h2>
                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
              </div>
              <div class="col-md-2 col-sm-2  tile">
                <span>Wasted Childrens</span>
                <h2>0</h2>
                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
              </div>
            </div>
          </div>
            <br/>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Timbang App
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url('assets/template') ?>/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="<?= base_url('assets/template') ?>/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url('assets/template') ?>/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?= base_url('assets/template') ?>/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?= base_url('assets/template') ?>/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?= base_url('assets/template') ?>/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- morris.js -->
    <script src="<?= base_url('assets/template') ?>/vendors/raphael/raphael.min.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/morris.js/morris.min.js"></script>
    <!-- gauge.js -->
    <script src="<?= base_url('assets/template') ?>/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?= base_url('assets/template') ?>/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Skycons -->
    <script src="<?= base_url('assets/template') ?>/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?= base_url('assets/template') ?>/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?= base_url('assets/template') ?>/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?= base_url('assets/template') ?>/vendors/moment/min/moment.min.js"></script>
    <script src="<?= base_url('assets/template') ?>/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?= base_url('assets/template') ?>/build/js/custom.min.js"></script>
    <script src = "https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>

  </body>
</html>