
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Timbang App | </title>

    <!-- Bootstrap -->
    <link href="<?= base_url('assets/template') ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url('assets/template') ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url('assets/template') ?>/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?= base_url('assets/template') ?>/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?= base_url('assets/template') ?>/build/css/custom.min.css" rel="stylesheet">
    <style>
/* Style all input fields */
input {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
}

/* Style the submit button */
input[type=submit] {
  background-color: #04AA6D;
  color: white;
}

/* Style the container for inputs */
.container {
  background-color: #f1f1f1;
  padding: 20px;
}

/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 2px;
  margin-top: 2px;
}

#message p {
  font-size: 15px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: "✔";
}

/* Add a red text color and an "x" when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: "✖";
}
</style>
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          <?php if (session()->getFlashdata('success') !== NULL) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if (session()->getFlashdata('error') !== NULL) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('error') ?>
            </div>
        <?php endif; ?>
          <form method = "post" action = "<?= base_url('register-user') ?>">
              <h1>Create Account</h1>
              <div class  = "row">
              <div class = "col-md-12">
                <input type="text" class="form-control" value= "<?= $session->get("firstname") ?>" placeholder="Firstname"  name = "firstname" />
              </div>
              <div class = "col-md-12">
                <input type="text" class="form-control" value= "<?= $session->get("username") ?>" placeholder="Lastname"  name = "lastname" />
              </div>
              <div class = "col-md-12">
                <input type="text" class="form-control" value= "<?= $session->get("middlename") ?>" placeholder="Middlename"  name = "middlename" />
              </div>
              <div class = "col-md-6">
                  <label for="">Age</label>
                <input type="text" class="form-control" value= "<?= $session->get("age") ?>" placeholder="Age"  name = "age" />
              </div>
              <div class = "col-md-6">
                <label for="">Birthdate</label>
                <input type="date" class="form-control" value= "<?= $session->get("birthdate") ?>"  name = "birthdate" />
              </div>
              <div class = "col-md-12">
                <input type="text" class="form-control" value= "<?= $session->get("address") ?>" placeholder="Address"  name = "address" />
              </div>
              <div class = "col-md-12">
                <input type="text" class="form-control" value= "<?= $session->get("position") ?>" placeholder="Position in Barangay"  name = "position" />
              </div>
              <div class = "col-md-12">
                <input value= "<?= $session->get("contact") ?>" type="text" class="form-control" placeholder="Contact No"  name = "contact" />
              </div>
              <div class = "col-md-12">
                <input type="text" class="form-control" value= "<?= $session->get("username") ?>" placeholder="Username" name  = "username"  />
              </div>
              <div class = "col-md-12">
                <input type="password" class="form-control" placeholder="Password" name = "password" id = "password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" />
                <div id="message" class = "col-md-12">
              <h6>Password must contain the following:</h6>
              <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
              <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
              <p id="number" class="invalid">A <b>number</b></p>
              <p id="length" class="invalid">Minimum <b>8 characters</b></p>
            </div>
              </div>
              <div class = "col-md-12">
                <label for="">Choose a Security Question</label>
                <select name="security_question" id="security_question" class = "form-control">
                  <option value=""></option>
                  <option value="What is your mother's maiden name?">What is your mother's maiden name?</option>
                  <option value="What elementary school did you attend?">What elementary school did you attend?</option>
                  <option value="What is the name of the town where you were born?">What is the name of the town where you were born?</option>
                </select>
              </div>
              <div class="div col-md-12">
                  <label for="">Answer</label>
                  <input type="text" name = "security_answer" class="form-control">
              </div>
              <div class = "col-md-12">
                <button type = "submit" class=  "btn btn-success btn-block">Submit</button>
              </div>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="<?= base_url('login') ?>" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                <p>©2021 All Rights Reserved. Timbang App</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
  </body>
</html>
<script>
var myInput = document.getElementById("password");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById("message").style.display = "none";
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
  }
  
  // Validate capital letters
  var upperCaseLetters = /[A-Z]/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove("invalid");
    capital.classList.add("valid");
  } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }
}
</script>
