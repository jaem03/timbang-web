
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Timbang App | </title>

    <!-- Bootstrap -->
    <link href="<?= base_url('assets/template') ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url('assets/template') ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- NProgress -->
    <link href="<?= base_url('assets/template') ?>/vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?= base_url('assets/template') ?>/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?= base_url('assets/template') ?>/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          <?php if (session()->getFlashdata('success') !== NULL) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if (session()->getFlashdata('error') !== NULL) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('error') ?>
            </div>
        <?php endif; ?>
            <form method = "post" action = "<?= base_url('login-user') ?>">
              <h1>Login</h1>
              <div>
                <input type="text" class="form-control" value= "<?= $session->get("username") ?>" placeholder="Username" required="" name = "username"/>
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" name = "password"/>
              </div>
              <div>
                <button class="btn btn-success btn-block" >Log in</button>
                <a class="reset_pass" href="<?= base_url('forgot-password') ?>">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="<?= base_url('register') ?>" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <p>©2021 All Rights Reserved. Timbang App</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">

           
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
