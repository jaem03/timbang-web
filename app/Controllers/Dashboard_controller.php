<?php

namespace App\Controllers;
use App\Models\Crud_model;

class Dashboard_controller extends BaseController
{
    public $session , $crud;
    public function __construct()
    {
        $this->crud = new Crud_model();
        $this->session = \Config\Services::session();
        helper(['form', 'url']);
    }
	public function index()
	{
        $data['session'] = $this->session;
        echo view('dashboard/templates/header');
        echo view('dashboard/main', $data);
        echo view('dashboard/templates/footer');
	}
    public function accounts(){
        $data['accounts'] = $this->crud->retrieve('users');
        $data['session'] = $this->session;
        echo view('dashboard/templates/header');
        echo view('dashboard/accounts', $data);
        echo view('dashboard/templates/footer');

    }

}
