<?php

namespace App\Controllers;

use App\Models\User_model;
use App\Models\Crud_model;

class User_controller extends BaseController
{
	public $crud , $validation , $user , $session;
    public function __construct()
    {
        $this->user = new User_model();
		$this->crud = new Crud_model();
		$this->validation =  \Config\Services::validation();
		$this->session = \Config\Services::session();
		
		helper(['form', 'url']);
    }
	public function register()
	{
		$data = array('firstname' => $this->request->getPost('firstname'),
					  'lastname' => $this->request->getPost('lastname'),
					  'middlename' => $this->request->getPost('middlename'),
					  'age' => $this->request->getPost('age'),
					  'birthdate' => $this->request->getPost('birthdate'),
					  'address' => $this->request->getPost('address'),
					  'position' => $this->request->getPost('position'),
					  'contact' => $this->request->getPost('contact'),
					  'username' => $this->request->getPost('username') ,
					  'password' => md5($this->request->getPost('password')),
					  'security_question' => $this->request->getPost('security_question'),
					  'security_answer' =>$this->request->getPost('security_answer')
		);
		$username = array('username' => $this->request->getPost('username'));
		$if_exists = $this->crud->retrieve_where('users' , $username);
		if($if_exists){
			$this->session->setFlashdata($data);
			return redirect()->route('register')->with('error' , "Username already exists");
		}else{
			$is_created = $this->crud->create('users', $data);
			if($is_created){
			
				return redirect()->route('register')->with('success' , "Registration Success");
			}
		}
	}
	public function login(){

		$data = array('username' => $this->request->getPost('username'));
		$if_exists = $this->crud->retrieve_where('users' , $data);
		if($if_exists){
			$credentials = array('username' => $this->request->getPost('username'),
			'password' => md5($this->request->getPost('password')));
			$if_valid = $this->crud->retrieve_where('users' , $credentials);
			if($if_valid){
				return redirect()->route('login')->with('success' , "Login Success");
			}else{
				return redirect()->route('login')->with('error' , "Incorrect Password");
			}

		}else{
			$this->session->setFlashdata($data);
			return redirect()->route('login')->with('error' , "Username does not exists.");
		}
	}
	public function search_username(){
		$data = array('username' => $this->request->getPost('username'));

		if(!$this->request->getPost('security_answer')){
			$if_exists = $this->crud->retrieve_where('users' , $data);
			if($if_exists){
				$question = "";
				foreach ($if_exists as $user) {
					$question = $user->security_question;
				}
				$user_data = array('username' => $user->username,
									'security_question' =>$user->security_question
			);
				$this->session->set($user_data);
				return redirect()->route('forgot-password')->with('success' , "Please answer the security question below.");
			}else{
				$this->session->setFlashdata($data);
				return redirect()->route('forgot-password')->with('error' , "Username not Found");
			}
		}
		else{
			$reset_data = array('username' => $this->request->getPost('username'),	
								'security_answer' => $this->request->getPost('security_answer')
			);
			$if_correct = $this->crud->retrieve_where('users' , $reset_data);
			if($if_correct){
				$this->session->set($data);
				return redirect()->route('set-password')->with('correct_answer' , "Security Question is Correct");
			}else{
				$this->session->setFlashdata($reset_data);
				return redirect()->route('forgot-password')->with('error_answer' , "Your answer is incorrect");
			}
		}
	}
	public function set_password(){
		$data = array('username' =>  $this->request->getPost('username'));
		$new_pass = array('password' => md5($this->request->getPost('new_password')));
			$update_data = $this->crud->update_where('users' , $new_pass , $data);
			if($update_data){
				return redirect()->route('login')->with('success' , "New password has been set successfuly");
			}
	}
	public function retrieve_all(){
		$get_all = $this->crud->retrieve('users');
		$data = array();
		foreach ($get_all as $dt) {
			# code...
			$nested_data = array();
			$nested_data[] = $dt->firstname ;
			$nested_data[] = $dt->lastname ;
			$nested_data[] = $dt->username ;
			$nested_data[] = $dt->status ;
			$data[] =  $nested_data;
		}
		echo json_encode($data);
	}
}
