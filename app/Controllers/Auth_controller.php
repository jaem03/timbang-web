<?php

namespace App\Controllers;

class Auth_controller extends BaseController
{
    public $session;
    public function __construct()
    {
        $this->session = \Config\Services::session();
        helper(['form', 'url']);
    }
	public function index()
	{
	}
    public function register(){
        $data['session'] = $this->session;
        return view('auth/register', $data);
    }
    public function login(){
        $data['session'] = $this->session;
        return view('auth/login' , $data);
    }
    public function forgot_password(){
        $data['session'] = $this->session;
        return view('auth/forgot_password' , $data);
    }
    public function set_password(){
        $data['session'] = $this->session;
        return view('auth/set_password' , $data);
    }
}
